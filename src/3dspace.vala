
using Matrices;

namespace Geometry3D {
	

	errordomain Wavefront {
		INVALID_INPUT,
		UNSUPPORTED
	}

	public class Space {
		internal Matrix matrix;
		internal Matrix normals;
		internal Face[] faces;
		public Vector lightVect {get; set; default = Vector(0,0,-1);}
		public Transformation view {get; set; default = null;}
		public int viewDistance {get; set; default = 5;}

		private Space() {
			faces = {};
		}

		internal delegate int CoordConverter(double d);

		public static double getScaleFactor(Matrix mat, bool reduceOnly = true) {
			double factor = 0.0;
			for (int i = 0; i < mat.width;i ++)
				for (int j = 0; j < 3; j ++)
					factor = double.max(factor, Math.fabs(mat[i,j]));
			factor = 1/factor;
			if (factor > 1.0 && reduceOnly)
				return 1.0;
			return factor;
		}

		public void transform(Transformation t) {
			t.applyTo(matrix);
			t.applyTo(normals);
		}

		public void draw(Image img) {
			Canvas can = (img is Canvas) ? (Canvas)img : new Canvas(img);
			stdout.printf("Surface : %u,%u\n", img.width, img.height);
			int w = (int)img.width;
			int h = (int)img.height;
			double scale = (int.min(w, h)-1)/2.0;
			stdout.printf("Scale : %f\n", scale);
			Transformation t = new Transformation.scale(scale, scale, scale).combine(new Transformation.translate(1.0, 1.0, 0));
			Matrix mat, unprojected, unprojNorms;
			if (view != null) {
				mat = view.multiply(matrix);
				unprojNorms = view.multiply(normals);
			} else {
				unprojNorms = new Transformation().multiply(normals);
				mat = new Transformation().multiply(matrix);
			}
			unprojected = new Transformation().multiply(mat); // Clone
			for (int i = 0; i < mat.width; i ++) {
				mat[i,0] = mat[i,0]/(-mat[i,2]+viewDistance)*(viewDistance-1);
				mat[i,1] = mat[i,1]/(-mat[i,2]+viewDistance)*(viewDistance-1);
			}
			mat = t.multiply(mat);

			Point p0, p1, p2;
			//PixelGenerator gen = PunctualLightGenerator(lightSource, false, Color(Color.WHITE), Color(0x202020));
			PixelGenerator gen = ParallelLightGenerator(lightVect, false, Color(Color.WHITE), Color(0x202020));
			//PixelGenerator gen = SolidColorGenerator(Color(0x808080));
			int n = mat.width;
			for (int i = 0; i < faces.length; i ++) {
				Face f = faces[i];
				double visibility = Vector(unprojNorms[i+n,0], unprojNorms[i+n,1], unprojNorms[i+n,2]).dot(Vector(0, 0, 1));
				if (visibility > 0) {
					p0 = Point((int)mat[f.pts[0],0], (int)mat[f.pts[0],1], mat[f.pts[0],2],
									unprojNorms[f.pts[0],0], unprojNorms[f.pts[0],1], unprojNorms[f.pts[0],2]);
					p1 = Point((int)mat[f.pts[1],0], (int)mat[f.pts[1],1], mat[f.pts[1],2],
									unprojNorms[f.pts[1],0], unprojNorms[f.pts[1],1], unprojNorms[f.pts[1],2]);
					p2 = Point((int)mat[f.pts[2],0], (int)mat[f.pts[2],1], mat[f.pts[2],2],
									unprojNorms[f.pts[2],0], unprojNorms[f.pts[2],1], unprojNorms[f.pts[2],2]);
					can.fillTriangle(p0, p1, p2, gen);
				}
			}
		}

		public static Space readWavefront(string filename) throws Error {
			File file = File.new_for_path(filename);

			var dis = new DataInputStream(file.read());
			string line;
			Point3D[] points = {Point3D(0,0,0)};
			Vector[] normals = {Vector(0,0,1)};
			Face[] faces = {};
			double max = 1.0;
			double x, y, z;
			int[] pts;
			var separator = new Regex("\\s+");

			while ((line = dis.read_line(null)) != null) {
				string[] parts = separator.split(line);
				line = line.escape("");
				if (parts.length < 1)
					continue;
				switch(parts[0]) {
					case "v":
						if (parts.length < 4 || parts.length > 5)
							throw new Wavefront.INVALID_INPUT("Invalid number of parts on 'v' line : %d", parts.length);
						x = double.parse(parts[1]);
						y = double.parse(parts[2]);
						z = double.parse(parts[3]);
						max = double.max(max, Math.fabs(x));
						max = double.max(max, Math.fabs(y));
						max = double.max(max, Math.fabs(z));
						stdout.printf("Parsed point %d:(%f,%f,%f)\n", points.length, x, y, z);
						points += Point3D(x, y, z);
						break;
					case "vn":
						if (parts.length < 4 || parts.length > 5)
							throw new Wavefront.INVALID_INPUT("Invalid number of parts on 'vn' line : %d", parts.length);
						x = double.parse(parts[1]);
						y = double.parse(parts[2]);
						z = double.parse(parts[3]);
						normals += Vector(x, y, z);
						break;
					case "f":
						if (parts.length < 4)
							throw new Wavefront.INVALID_INPUT("Face with less than 3 vertices");
						pts = {};
						for (int i = 1; i < parts.length; i ++)
							if (parts[i].length > 0)
								pts += int.parse(parts[i].contains("/") ? parts[i].substring(0, parts[i].index_of("/")) : parts[i]);
						if (pts.length == 3)
							faces += new Face(pts[0], pts[1], pts[2]);
						else {
							Face[] tmp = Face.splitPolygon(pts);
							foreach (Face f in tmp)
								faces += f;
						}
						break;
				}
			}

			stdout.printf("Max : %f\n", max);

			Space sp = new Space();

			sp.matrix = new Matrix.points(points.length);
			sp.normals = new Matrix(points.length+faces.length, 4);

			double imax = 1.0/max;
			for (int i = 0; i < faces.length; i ++) {
				Face f = faces[i];
				Vector v = Vector.plane_normal(points[f.pts[0]], points[f.pts[1]], points[f.pts[2]]);
				sp.normals[points.length+i,0] = v.x;
				sp.normals[points.length+i,1] = v.y;
				sp.normals[points.length+i,2] = v.z;
			}
			
			for (int i = 0; i < points.length; i ++) {
				sp.matrix[i,0] = points[i].x*imax;
				sp.matrix[i,1] = points[i].y*imax;
				sp.matrix[i,2] = points[i].z*imax;
				sp.normals[i,0] = normals[i].x;
				sp.normals[i,1] = normals[i].y;
				sp.normals[i,2] = normals[i].z;
			}

			stdout.printf("Parsed file. Found %d points, %d faces\n", points.length-1, faces.length);
			faces.resize(faces.length);
			sp.faces = faces;
			return sp;
		}

		internal class Face {
			internal int[] pts;
			internal bool display;

			internal Face(int p1, int p2, int p3) {
				pts = {p1, p2, p3};
				display = true;
			}

			internal static Face[] splitPolygon(int[] pts) {
				var faces = new Face[pts.length-2];
				uint n = pts.length;
				int p1 = pts[0],p2=pts[1],p3=pts[1];
				for (int i = 0; i < n-2; i ++) {
					p2=p3;
					p3=pts[(i+2)%n];
					stdout.printf("Face : ({%d},{%d},{%d})\n", p1, p2, p3);
					faces[i] = new Face(p1, p2, p3);
				}
				stdout.printf("\n");
				return faces;
			}
		}

	}
	
}
