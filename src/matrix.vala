
namespace Matrices {
	
	[Immutable]
	public struct Vector {
		public double x {get; private set;}
		public double y {get; private set;}
		public double z {get; private set;}

		public Vector(double x, double y, double z, bool normalize = true) {
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public Vector.plane_normal(Point3D p0, Point3D p1, Point3D p2) {
			var v1 = Vector.from_points(p0, p1), v2 = Vector.from_points(p0, p2);
			x = v1.y*v2.z-v1.z*v2.y;
			y = v1.z*v2.x-v1.x*v2.z;
			z = v1.x*v2.y-v1.y*v2.x;
		}

		public Vector.with_array(double[] coord) {
			this.x = coord[0];
			this.y = coord[1];
			this.z = coord[2];
		}

		public Vector.from_points(Point3D p1, Point3D p2) {
			x = p2.x-p1.x;
			y = p2.y-p1.y;
			z = p2.z-p1.z;
		}

		public double dot(Vector v) {
			return x*v.x + y*v.y +z*v.z;
		}

		public Vector cross(Vector v) {
			return Vector(y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x);
		}

		public double getLength() {
			return Math.sqrt(x*x + y*y + z*z);
		}

		public Vector normalize() {
			double l = getLength();
			return (l > 0) ? Vector(x/l, y/l, z/l) : Vector(0,0,0);
		}

		public static Vector getNormal(Point3D p1, Point3D p2, Point3D p3) {
			return Vector.from_points(p1, p2).cross(Vector.from_points(p1, p3)).normalize();
		}
	}

	public struct Point3D {
		public double x {get; private set;}
		public double y {get; private set;}
		public double z {get; private set;}

		public Point3D(double x, double y, double z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}

	public class Matrix {
		public int width {get {return mat.length[0];} private set{}}
		public int height {get {return mat.length[1];} private set{}}
		public double[,] mat;

		public Matrix(int w, int h) requires(w > 0 && h > 0) {
			mat = new double[w,h];
			for (int i = 0; i < w; i ++)
				for (int j = 0; j < h; j ++)
					mat[i,j] = 0;
		}

		public Matrix.points(int nb) {
			mat = new double[nb,4];
			width = nb;
			for (int i = 0; i < nb; i ++)
				for (int j = 0; j < 4; j ++)
					mat[i,j] = (j < 3 ? 0 : 1);
		}

		public Matrix.with_array(double[,] array, bool copy = true) {
			if (copy) {
				Memory.copy(mat, array, width*height*sizeof(double));
				mat = new double[array.length[0],array.length[1]];
			} else
				mat = array;
		}

		public Matrix multiply(Matrix m) requires(width == m.height) {
			int w = m.width, h = height;
			double[,] nmat = new double[w,h];
			double v;
			for (int i = 0; i < w; i ++)
				for (int j = 0; j < h; j ++) {
					v = 0.0;
					for (int k = 0; k < height; k ++)
						v += m.mat[i,k]*mat[k,j];
					nmat[i,j] = v;
				}
			return new Matrix.with_array(nmat, false);
		}

		protected void copy(Matrix m) requires(m.width == width && m.height == height) {
			Memory.copy(mat, m.mat, width*height*sizeof(double));
		}

		public Matrix add(Matrix m) requires (m.width == width && m.height == height) {
			var res = new Matrix(width, height);
			for (int i = 0; i < width; i ++)
				for (int j = 0; j < height; j ++)
					res.mat[i,j] = mat[i,j]+m.mat[i,j];
			return res;
		}

		public double get(int i, int j) {
			return mat[i,j];
		}

		public void set(int i, int j, double v) {
			mat[i,j] = v;
		}

		public Matrix transpose() {
			Matrix m = new Matrix(height, width);
			for (int i = 0; i < width; i ++)
				for (int j = 0; j < height; j ++)
					m.mat[j,i] = mat[i,j];
			return m;
		}

		public void printContent() {
			for (int j = 0; j < height; j ++) {
				for (int i = 0; i < width; i ++) {
					stdout.printf("%6.2f ", mat[i,j]);
				}
				stdout.printf("\n");
			}
			stdout.printf("\n");
		}
	}

	public class Transformation : Matrix {
		public Transformation() {
			base(4, 4);
			for (int i = 0; i < 4; i ++)
				this[i,i] = 1;
		}

		public Transformation.translate(double dx, double dy, double dz) {
			this();
			this[3,0] = dx;
			this[3,1] = dy;
			this[3,2] = dz;
		}

		public Transformation.scale(double sx, double sy = double.INFINITY, double sz = double.INFINITY) {
			this();
			this[0,0] = sx;
			this[1,1] = (sy == double.INFINITY) ? sx : sy;
			this[2,2] = (sz == double.INFINITY) ? sx : sz;
		}

		public Transformation.shear(double yx, double zx, double xy, double zy, double xz, double yz) {
			this();
			this[1,0] = yx;
			this[2,0] = zx;
			this[0,1] = xy;
			this[2,1] = zy;
			this[0,2] = xz;
			this[1,2] = yz;
		}

		public Transformation.rotate(double angle, Axis axis) {
			this();
			double cosa = Math.cos(angle), sina = Math.sin(angle);
			switch (axis) {
				case Axis.X:
				this[1,1] = cosa;
				this[2,2] = cosa;
				this[1,2] = sina;
				this[2,1] = -sina;
				break;
				case Axis.Y:
				this[0,0] = cosa;
				this[2,2] = cosa;
				this[2,0] = sina;
				this[0,2] = -sina;
				break;
				case Axis.Z:
				this[0,0] = cosa;
				this[1,1] = cosa;
				this[0,1] = sina;
				this[1,0] = -sina;
				break;
			}
		}

		public void applyTo(Matrix mat) requires (mat.height == 4) {
			Matrix m = multiply(mat);
			mat.copy(m);
		}

		public Point3D applyToPoint(Point3D point) {
			double[] p = {point.x, point.y, point.z, 1}, np = {0,0,0};
			for (int i = 0; i < 3; i ++) {
				for (int j = 0; j < 4; j ++)
					np[i] += p[j]*this[j,i];
			}
			return Point3D(np[0], np[1], np[2]);
		}

		public Transformation combine(Transformation? t) {
			Transformation res = new Transformation();
			if (t != null)
				res.copy(multiply(t));
			else
				res.copy(this);
			return res;
		}

	}

	public enum Axis {
		X, Y, Z;
	}
}


