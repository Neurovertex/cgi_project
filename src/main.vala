
using TGA;
using Geometry3D;
using Matrices;

int main(string[] args) {
	try {
		int width = 800, height = 800;
		TGAImage img = new TGAImage(width*2, height*2, TGAImage.RGB);
		img.setOrigin(TGA.BOTTOM_LEFT);
		Canvas canvas1 = new Canvas(new SubImage(img, 0, 0, width, height)), canvas2 = new Canvas(new SubImage(img, 0, height, width, height)), 
						canvas3 = new Canvas(new SubImage(img, width, 0, width, height)), canvas4 = new Canvas(new SubImage(img, width, height, width, height));
		if (args.length > 1) {
			Transformation t = null;
			if (args.length > 3) {
				double angle = int.parse(args[3])*Math.PI/180;
				Axis a = Axis.X;
				switch (args[2]) {
				case "x":
					a = Axis.X;
					break;
				case "y":
					a = Axis.Y;
					break;
				case "z":
					a = Axis.Z;
					break;
				}
				t = new Transformation.rotate(angle, a);
				t.printContent();
			}
			stdout.printf("File : %s\n", args[1]);
			var space = Space.readWavefront(args[1]);
			space.lightVect = Vector(0.3, -0.5, -0.6);
			space.view = t;
			space.draw(canvas1);
			space.lightVect = Vector(-0.5, 0.3, -0.3);
			space.draw(canvas2);
			space.viewDistance = 3;
			space.lightVect = Vector(0.3, -0.5, -0.6);
			space.view = new Transformation.rotate(3*Math.PI/8, Axis.Y).combine(t);
			space.draw(canvas3);
			space.lightVect = Vector(-0.5, 0.3, -0.3);
			space.draw(canvas4);
			//lcanvas.fillTriangle(Point(100, 100, 0), Point(100, 700, 100), Point(700, 100, -100), SolidColorGenerator(Color(Color.WHITE)));
		}

		/*var rand = new Rand();
		Point3D p0, p1, p2, light = Point3D(rand.int_range(width/4, 3*width/4), rand.int_range(height/4, 3*height/4), -200);
		Vector normal, lightVect = Vector(rand.int_range(-50,50), rand.int_range(-50,50), 100).normalize();
		PixelGenerator col = ParallelLightGenerator(lightVect, true), solid;
		p1 = Point3D(rand.int_range(0,width), rand.int_range(0,width), rand.int_range(-100, 100));
		p2 = Point3D(rand.int_range(0,width), rand.int_range(0,width), rand.int_range(-100, 100));
		for (int i = 0; i < 5; i ++) {
			p0 = p1;
			p1 = p2;
			p2 = Point3D(rand.int_range(0,width), rand.int_range(0,width), rand.int_range(-100, 100));
			normal = Vector.plane_normal(p0, p1, p2);
			solid = SolidColorGenerator(Color.with_rgb((uint8)rand.int_range(20,255), (uint8)rand.int_range(20,255), (uint8)rand.int_range(20,255)));
			lcanvas.fillTriangle(Point.fromPoint3D(p0, normal), Point.fromPoint3D(p1, normal), Point.fromPoint3D(p2, normal), solid);
			rcanvas.fillTriangle(Point.fromPoint3D(p0, normal), Point.fromPoint3D(p1, normal), Point.fromPoint3D(p2, normal), col);
		}*/
		/*for (int i = 0; i < 9; i ++)
			lcanvas.line(Point(width/2, height/2, -200), Point((int)(width/2+lightVect.x*100), (int)(height/2+lightVect.y*100), -200), SolidColorGenerator(Color(Color.RED)));*/
		img.writeTGA("dump.tga");
		stdout.printf("Successful. Exitting.\n");
	} catch (Error e) {
		stdout.printf("Error writing the file : %s\n", e.message);
		return 1;
	}
	return 0;
}
