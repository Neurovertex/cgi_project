
using Matrices;

[Immutable]
public struct Color {
	public const uint32 BLACK =	0x000000;
	public const uint32 WHITE =	0xFFFFFF;
	public const uint32 RED =	0xFF0000;
	public const uint32 GREEN =	0x00FF00;
	public const uint32 BLUE =	0x0000FF;
	public uint32 argb {get; private set; }
	public uint8 a {get {return (uint8)((argb >> 24) & 0xFF); } }
	public uint8 r {get {return (uint8)((argb >> 16) & 0xFF); } }
	public uint8 g {get {return (uint8)((argb >> 8) & 0xFF); } }
	public uint8 b {get {return (uint8)(argb & 0xFF); }	}

	public Color(uint32 argb) {
		this.argb = argb;
	}

	public Color.with_argb(uint8 a, uint8 r, uint8 g, uint8 b) {
		this.argb = (a<<24) | (r<<16) | (g<<8) | b;
	}

	public Color.with_rgb(uint8 r, uint8 g, uint8 b) {
		this.argb = (0xFF<<24) | (r<<16) | (g<<8) | b;
	}

	public string toString() {
		return "#%06X".printf(argb);
	}
}

public interface Image : GLib.Object {
	public abstract void setPixel(uint x, uint y, Color c);
	public abstract Color getPixel(uint x, uint y);
	public abstract uint width {get; protected set;}
	public abstract uint height {get; protected set;}
}

public class Canvas : Image, GLib.Object {
	private Image img;
	private double[,] zbuffer;
	public uint width {get { return img.width;} protected set{}}
	public uint height {get { return img.height;} protected set{}}

	public Canvas(Image img) {
		this.img = img;
		zbuffer = new double[img.width, img.height];
		for (int x = 0; x < img.width; x ++)
			for (int y = 0; y < img.height; y ++)
				zbuffer[x,y] = -double.INFINITY;
	}

	public void setPixel(uint x, uint y, Color c) {
		img.setPixel(x, y, c);
	}

	public void setPixelZ(uint x, uint y, double z, Color c) {
		if (x < width && y < height) {
			if (z+0.0001 >= zbuffer[x,y]) {
				img.setPixel(x, y, c);
				zbuffer[x, y] = z;
			}
		}
	}

	public Color getPixel(uint x, uint y) {
		return img.getPixel(x, y);
	}

	private void swapPoint(ref Point p1, ref Point p2) {
		Point tmp = p1;
		p1 = p2;
		p2 = tmp;
	}

	public void line(Point p0, Point p1, PixelGenerator generate) {
		bool steep = (p1.y-p0.y).abs() > (p1.x-p0.x).abs();
		if (steep) {
			p0 = Point(p0.y, p0.x, p0.z, p0.u, p0.v, p0.w);
			p1 = Point(p1.y, p1.x, p1.z, p1.u, p1.v, p1.w);
		}
		if (p0.x > p1.x)
			swapPoint(ref p0, ref p1);

		if (p0.x == p1.x) {
			setPixelZ(p0.x, p0.y, p0.z, generate(p0));
			setPixelZ(p0.x, p0.y, p0.z, generate(p1));
			return;
		}

		Component compy = Component(p0.x, p0.y, p1.x, p1.y);
		VectorComponent[] vector = {VectorComponent(p0.x, p0.z, p1.x, p1.z), VectorComponent(p0.x, p0.u, p1.x, p1.u), VectorComponent(p0.x, p0.v, p1.x, p1.v), VectorComponent(p0.x, p0.w, p1.x, p1.w)};

		for (int x = p0.x; x <= p1.x; x ++) {
			Point p = (steep) ? Point(compy.val, x, vector[0].val, vector[1].val, vector[2].val, vector[3].val) : Point(x, compy.val, vector[0].val, vector[1].val, vector[2].val, vector[3].val);
			setPixelZ(p.x, p.y, p.z, generate(p));

			compy.increment();
			for (int i = 0; i < vector.length; i ++)
				vector[i].increment();
		}
	}

	public void drawTriangle(Point p0, Point p1, Point p2, PixelGenerator gen) {
		line(p0, p1, gen);
		line(p1, p2, gen);
		line(p0, p2, gen);
	}

	public void fillTriangleSolid(int x0, int y0, int x1, int y1, int x2, int y2, Color c) {
		var p0 = Point(x0, y0, int.MIN), p1 = Point(x1, y1, int.MIN), p2 = Point(x2, y2, int.MIN);
		var solid = SolidColorGenerator(c);
		fillTriangle(p0, p1, p2, solid);
	}


	public void fillTriangle(Point p0, Point p1, Point p2, PixelGenerator gen) {
		if (p1.x < p0.x)
			swapPoint(ref p0, ref p1);
		if (p2.x < p1.x)
			swapPoint(ref p2, ref p1);
		if (p1.x < p0.x)
			swapPoint(ref p0, ref p1);

		Component[] comps = {Component(p0.x, p0.y, p1.x, p1.y), Component(p0.x, p0.y, p2.x, p2.y)};
		VectorComponent[] vector = {VectorComponent(p0.x, p0.z, p1.x, p1.z), VectorComponent(p0.x, p0.u, p1.x, p1.u), VectorComponent(p0.x, p0.v, p1.x, p1.v), VectorComponent(p0.x, p0.w, p1.x, p1.w),
									VectorComponent(p0.x, p0.z, p2.x, p2.z), VectorComponent(p0.x, p0.u, p2.x, p2.u), VectorComponent(p0.x, p0.v, p2.x, p2.v), VectorComponent(p0.x, p0.w, p2.x, p2.w)};
		if (p0.x == p2.x) { // Flat triangle
			line(p0, p1, gen);
			line(p1, p2, gen);
			line(p0, p2, gen);
			return;
		}

		if (p0.x == p1.x) {
			comps[0] = Component(p1.x, p1.y, p2.x, p2.y);
			vector[0] = VectorComponent(p1.x, p1.z, p2.x, p2.z);
			vector[1] = VectorComponent(p1.x, p1.u, p2.x, p2.u);
			vector[2] = VectorComponent(p1.x, p1.v, p2.x, p2.v);
			vector[3] = VectorComponent(p1.x, p1.w, p2.x, p2.w);
		}

		for (int x = p0.x; x <= p2.x; x ++) {
			var pa = Point(x, comps[0].val, vector[0].val, vector[1].val, vector[2].val, vector[3].val);
			var pb = Point(x, comps[1].val, vector[4].val, vector[5].val, vector[6].val, vector[7].val);
			line(pa, pb, gen);

			if (x < p2.x) {
				for (int i = 0; i < comps.length; i ++)
					comps[i].increment();
				for (int i = 0; i < vector.length; i ++)
					vector[i].increment();
				if (x+1 == p1.x) {
					comps[0] = Component(p1.x, p1.y, p2.x, p2.y);
					vector[0] = VectorComponent(p1.x, p1.z, p2.x, p2.z);
					vector[1] = VectorComponent(p1.x, p1.u, p2.x, p2.u);
					vector[2] = VectorComponent(p1.x, p1.v, p2.x, p2.v);
					vector[3] = VectorComponent(p1.x, p1.w, p2.x, p2.w);
				}
			}
		}
	}

	public void fillRectangle(int x, int y, int w, int h, Color c) requires (x > 0 && x+w < width && y > 0 && y+h <= 0) {
		for (int i = x; i < x+w; i ++)
			for (int j = y; j < y+h; j ++)
				setPixel(i,j,c);
	}

	public void drawPolygon(Point[] points, PixelGenerator gen) {
		int n = points.length;
		for (int i = 0; i < n; i ++)
			line(points[i], points[(i+1)%n], gen);
	}

	private struct Component {
		int dx;
		int dv;
		int val;
		int err;
		int inc;

		internal Component(int x0, int v0, int x1, int v1) {
			dv = (v1-v0).abs()*2;
			dx = x1-x0;
			inc = (v1 > v0) ? 1 : -1;
			err = 0;
			val = v0;
		}

		internal void increment() {
			err += dv;
			while (err >= dx) {
				val += inc;
				err -= 2*dx;
			}
		}
	}

	private struct VectorComponent {
		double val;
		double inc;

		internal VectorComponent(int x0, double v0, int x1, double v1) {
			inc = (v1-v0)/(x1-x0);
			val = v0;
		}

		internal void increment() {
			val += inc;
		}
	}

	public void printZBuffer(Image img) requires(img.width == width && img.height == height) {
		double min = double.MAX, max = double.MIN;

		for (int i = 0; i < width; i ++)
			for (int j = 0; j < height; j ++)
				if (zbuffer[i,j] != -double.INFINITY) {
					if (zbuffer[i,j] < min)
						min = zbuffer[i,j];
					if (zbuffer[i,j] > max)
						max = zbuffer[i,j];
				}
		int offset = 16;
		double scale = (255-offset)/(max-min);
		Color black = Color(Color.BLACK);
		for (int i = 0; i < width; i ++)
			for (int j = 0; j < height; j ++) {
				if (zbuffer[i,j] == -double.INFINITY)
					img.setPixel(i,j,black);
				else {
					int level = (int)(scale*(zbuffer[i,j]-min))+offset;
					uint8 r = (uint8)((level*2).clamp(offset,255)), g = (uint8)((level*2-(255-offset)).clamp(offset,255)), b = g;
					var col = Color.with_rgb(r,g,b);
					img.setPixel(i,j,col);
				}
			}

		stdout.printf("min : %f, max : %f\n", min, max);
	}

}

public delegate Color PixelGenerator(Point p);

public PixelGenerator ParallelLightGenerator(Vector light, bool bothSides=false, Color max = Color(Color.WHITE), Color min = Color(Color.BLACK)) {
	double dr = max.r - min.r, dg = max.g - min.g, db = max.b - min.b;
	light = light.normalize();
	if (!bothSides)
		return (p) => {
			var vect = Vector(p.u, p.v, p.w).normalize();
			double coeff = -vect.dot(light);
			coeff = coeff.clamp(0, 1);
			coeff = Math.sqrt(2*coeff-coeff*coeff);
			return Color.with_rgb((uint8)(min.r+coeff*dr), (uint8)(min.g+coeff*dg), (uint8)(min.b+coeff*db));
		};
	else
		return (p) => {
			var vect = Vector(p.u, p.v, p.w).normalize();
			double coeff = -vect.dot(light);
			coeff = Math.fabs(coeff).clamp(0, 1);
			coeff = Math.sqrt(2*coeff-coeff*coeff);
			return Color.with_rgb((uint8)(min.r+coeff*dr), (uint8)(min.g+coeff*dg), (uint8)(min.b+coeff*db));
		};
}

public PixelGenerator PunctualLightGenerator(Point3D source, bool bothSides=false, Color max = Color(Color.WHITE), Color min = Color(Color.BLACK)) {
	double dr = max.r - min.r, dg = max.g - min.g, db = max.b - min.b;
	if (!bothSides)
		return (p) => {
			var vect = Vector(p.u, p.v, p.w).normalize();
			var light = Vector.from_points(Point3D(p.x, p.y, p.z), source).normalize();
			double coeff = -vect.dot(light).clamp(0,1);
			coeff = Math.sqrt(2*coeff-coeff*coeff);
			var c = Color.with_rgb((uint8)(min.r+coeff*dr), (uint8)(min.g+coeff*dg), (uint8)(min.b+coeff*db));
			//	stdout.printf("%d:%d, %f, %s\n", p.x, p.y, coeff, c.toString());
			return c;
		};
	else
		return (p) => {
			var vect = Vector(p.u, p.v, p.w).normalize();
			var light = Vector.from_points(Point3D(p.x, p.y, p.z), source).normalize();
			double coeff = -vect.dot(light);
			coeff = Math.fabs(coeff).clamp(0,1);
			coeff = Math.sqrt(2*coeff-coeff*coeff);
			return Color.with_rgb((uint8)(min.r+coeff*dr), (uint8)(min.g+coeff*dg), (uint8)(min.b+coeff*db));
		};
}

public PixelGenerator SolidColorGenerator(Color c) {
	return (p) => {
		return c;
	};
}

public struct Point {
	public int x {get; private set;}
	public int y {get; private set;}
	public double z {get; private set;}

	public double u {get; private set;}
	public double v {get; private set;}
	public double w {get; private set;}

	public Vector normal {get {return Vector(u, v, w);} private set{}}

	public Point(int x, int y, double z, double u = 1, double v = 0, double w = 0) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.u = u;
		this.v = v;
		this.w = w;
	}

	public Point.with_vector(int x, int y, double z, Vector v) {
		this(x, y, z, v.x, v.y, v.z);
	}

	public Point.fromPoint3D(Point3D p, Vector v = Vector(0,0,0)) {
		this((int)p.x, (int)p.y, p.z, v.x, v.y, v.z);
	}

	public Point3D getPoint3D() {
		return Point3D(x, y, z);
	}
}


public class SubImage : Image, GLib.Object {
	private Image img;
	private uint marginx;
	private uint marginy;
	public uint width {get; protected set;}
	public uint height {get; protected set;}

	public SubImage(Image img, uint xm, uint ym, uint w, uint h) requires (w+xm <= img.width && h+ym <= img.height && w > 0 && h > 0) {
		this.img = img;
		this.marginx = xm;
		this.marginy = ym;
		this.width = w;
		this.height = h;
	}

	public SubImage.with_margins(Image img, uint xm, uint ym, uint xmopp, uint ymopp) requires (xm+xmopp < width && ym+ymopp < height) {
		this.img = img;
		this.marginx = xm;
		this.marginy = ym;
		this.width = img.width - (xm+xmopp);
		this.height = img.height - (ym+ymopp);
	}

	public void setPixel(uint x, uint y, Color c) requires(x < width && y < height)  {
		img.setPixel(x+marginx, y+marginy, c);
	}

	public Color getPixel(uint x, uint y) requires (x < width && y < height) {
		return img.getPixel(x+marginx, y+marginy);
	}
}


