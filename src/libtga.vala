

namespace TGA {
	
	public const uint8 BOTTOM_LEFT=0;
	public const uint8 BOTTOM_RIGHT=1;
	public const uint8 TOP_LEFT=2;
	public const uint8 TOP_RIGHT=3;


	public class TGAImage : Image, GLib.Object {
		private uint8[] imgdata;
		public uint width {get; protected set;}
		public uint height {get; protected set;}
		public uint8 bytespp {get; private set;}
		private int8 rotation;

		public static const uint8 RGB=3;
		public static const uint8 ARGB=4;

		public TGAImage(uint w, uint h, uint8 bpp) {
			this.width = w;
			this.height = h;
			this.bytespp = bpp;
			this.rotation = 0;
			imgdata = new uint8[width*height*bytespp];
			Memory.set(imgdata, 0, imgdata.length);
			stdout.printf("Allocated %d bytes for image\n", imgdata.length);
		}

		public void setPixel(uint x, uint y, Color color) requires(x < width && y < height) {
			uint32 col = color.argb;
			for (uint i = 0; i < bytespp; i ++)
				setp(x, y, i, (uint8)(col >> (8*i) & 0xFF));
		}

		public Color getPixel(uint x, uint y) requires(x <= width && y <= height) {
			uint32 argb = 0;
			for (uint i = 0; i < bytespp; i ++)
				argb |= getp(x, y, i)<<(8*i);
			return Color(argb);
		}

		public void setOrigin(uint8 origin) {
			rotation = origin%4;
		}

		public uint getWidth() {
			return width;
		}

		public uint getHeight() {
			return height;
		}

		private void setp(uint x, uint y, uint b, uint8 v) {
			imgdata[(x+width*y)*bytespp+b] = v;
		}
		
		private uint8 getp(uint x, uint y, uint b) {
			return imgdata[(x+width*y)*bytespp+b];
		}


		public void writeTGA(string filename) throws Error {
			var file = File.new_for_path(filename);
			if (file.query_exists())
				file.delete();
			var dos = new DataOutputStream(file.create(FileCreateFlags.REPLACE_DESTINATION));
			dos.byte_order = DataStreamByteOrder.LITTLE_ENDIAN;
			uint32 offset = 0;
			dos.put_byte((uint8)sizeof(uint64));				// ID field length
			dos.put_byte(0);									// Colormap type (none)
			dos.put_byte(2);									// Image type (true-color uncompressed)
			offset += 3;
			offset += (uint32)dos.write(new uint8[]{0,0,0,0,0});
			dos.put_uint16(0); offset+=2;						// X Origin
			dos.put_uint16(0); offset+=2;						// Y Origin
			dos.put_uint16((uint16)width); offset+=2;					// Image width (duh ...)
			dos.put_uint16((uint16)height); offset+=2;					// Image height
			dos.put_byte(bytespp <<3); offset++;				// Bytes per pixel
			dos.put_byte((uint8)rotation<<4); offset++;							// Direction
			dos.put_uint64((uint64)get_real_time());offset +=8;					// ID Field

			dos.write_all(imgdata, null); offset += imgdata.length;

			string signature = "TRUEVISION-XFILE.";
			dos.put_uint32(0); offset += 4;						// Extension area offset - non-existant
			dos.put_uint32(0); offset += 4;						// Dev area offset - non-existant
			dos.put_string(signature);offset+=signature.length;		// Signature
			dos.put_byte(0); offset++;							// Final NULL
			stdout.printf("Wrote %u bytes\n", offset);

			dos.flush();
			dos.close();
		}

		public void printData() {
			for (uint x = 0; x < width; x ++) {
				for (uint y = 0; y < height; y ++) {
					stdout.printf("(%02X", getp(x, y, 0));
					for (uint b = 1; b < bytespp; b++)
						stdout.printf(",%02X", getp(x, y, b));
					stdout.printf(") ");
				}
				stdout.printf("\n");
			}
		}
	}

}
