DESTDIR=bin/
TARGET=main

CFLAGS=-pthread $(shell pkg-config --cflags --libs glib-2.0 gio-2.0)
VALACARGS:=--pkg gio-2.0 -X -w -X -lm

SRCFILES=$(shell find src -name "*.vala")

all: $(DESTDIR)$(TARGET)

$(DESTDIR)$(TARGET): $(DESTDIR) $(SRCFILES)
	@cp $(SRCFILES) bck/
	valac $(VALACARGS) $(SRCFILES) -o $@

$(DESTDIR):
	mkdir -p $@

debug: $(DESTDIR) $(SRCFILES)
	valac -C -g $(VALACARGS) $(SRCFILES) -d csrc
	cc $(CFLAGS) -g csrc/src/* -o $(DESTDIR)$@
C:
	valac --pkg gio-2.0 -d csrc -C $(SRCFILES)

clean:
	-@rm bin/* &>/dev/null
